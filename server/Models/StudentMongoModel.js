const { Schema } = require("mongoose");

const StudentSchema = new Schema({
    name: String,
    email: String,
    regime: String,
    number: Number,
    favorite_color: String,
    createdAt: {
        type: Date,
        default: Date.now
    }
});

module.exports = MONGOOSE.model('dgc_students', StudentSchema);