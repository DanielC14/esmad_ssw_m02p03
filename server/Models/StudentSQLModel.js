const {
    DataTypes
} = require("sequelize");

const Model = SEQUELIZE.define('dgc_students', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    regime: DataTypes.CHAR(2),
    number: DataTypes.INTEGER,
    favorite_color: DataTypes.STRING
});

module.exports = Model;