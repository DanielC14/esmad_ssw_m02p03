const {
    validationResult,
    check
} = require('express-validator');
const Student = require("../Models/StudentSQLModel");


exports.getAll = (request, response) => {

    Student.findAll().then(students => {
        if (students.length <= 0)
            return response.status(404).send("Students not found!");
        else
            return response.status(200).send(students);
    }).catch(error => {
        return response.status(400).send(error);
    });


}

exports.getById = (request, response) => {

    const result = validationResult(request);
    if (!result.isEmpty())
        return response.send({
            errors: result.array()
        });

    let id = request.params.id;

    Student.findOne({
        where: {
            id: id
        }
    }).then(student => {
        if (!student)
            return response.status(404).send("Student not found!");
        else
            return response.status(200).send(student);
    }).catch(error => {
        return response.status(400).send(error);
    });

}

exports.create = (request, response) => {

    const result = validationResult(request);
    if (!result.isEmpty())
        return response.send({
            errors: result.array()
        });

    let new_student = {
        name: request.body.name,
        email: request.body.email,
        regime: request.body.regime,
        number: request.body.number,
        favorite_color: request.body.favorite_color
    }

    Student.create(new_student)
        .then(student => {
            return response.status(201).send(student);
        }).catch(error => {
            return response.status(400).send(error);
        });

}

exports.update = async (request, response) => {

    await check('id').not().isEmpty().withMessage('An id must be provided').isInt().withMessage('The id must be an int!').toInt().run(request)

    await check('name').not().isEmpty().withMessage('Name must not be empty').trim().escape().run(request)
    await check('email').optional().isEmail().withMessage('Invalid email').normalizeEmail().trim().run(request)
    await check('regime').optional().isIn(['PL', 'DR']).withMessage('Regime must be "PL" or "DR"').run(request)
    await check('number').optional().isLength({
        min: 7,
        max: 7
    }).withMessage('Number length must be 7').isNumeric().withMessage('Only numbers are allowed').run(request)
    await check('register_date').optional().isDate().withMessage('Invalid register date').toDate().run(request)
    await check('favorite_color').optional().isHexColor().withMessage('Invalid favorite color, must be hexcolor').run(request)

    const result = validationResult(request);
    if (!result.isEmpty())
        return response.send({
            errors: result.array()
        });



    let id = request.params.id;

    let student_update = {
        name: request.body.name,
        email: request.body.email,
        regime: request.body.regime,
        number: request.body.number,
        register_date: request.body.register_date,
        favorite_color: request.body.favorite_color
    };

    Student.update(student_update, {
        where: {
            id: id
        }
    }).then((number_updated) => {
        if (!number_updated[0])
            return response.status(404).send("Student not found!");
        else
            return response.status(200).send('Student updated!');
    }).catch(error => {
        return response.status(400).send(error);
    });

}

exports.delete = (request, response) => {

    const result = validationResult(request);
    if (!result.isEmpty())
        return response.send({
            errors: result.array()
        });

    let id = request.params.id;

    Student.destroy({
        where: {
            id: id
        }
    }).then((number_deleted) => {
        if (!number_deleted)
            return response.status(404).send("Student not found!");
        else
            return response.status(200).send('Student deleted!');
    }).catch(error => {
        return response.status(400).send(error);
    });
}