const express = require("express");
let router = express.Router();
const Controller = require("../Controllers/StudentMongoController");

const {
    param,
    body
} = require('express-validator');

router.route("/")
    .get(Controller.getAll)
    .post([
        body('name').not().isEmpty().withMessage('Name must not be empty').trim().escape(),
        body('email').optional().isEmail().withMessage('Invalid email').normalizeEmail().trim(),
        body('regime').optional().isIn(['PL', 'DR']).withMessage('Regime must be "PL" or "DR"'),
        body('number').optional().isLength({
            min: 7,
            max: 7
        }).withMessage('Number length must be 7').isNumeric().withMessage('Only numbers are allowed'),
        body('register_date').optional().isDate().withMessage('Invalid register date').toDate(),
        body('favorite_color').optional().isHexColor().withMessage('Invalid favorite color, must be hexcolor')
    ], Controller.create)

router.route("/:id")
    .get([param("id").not().isEmpty().withMessage('An _id must be provided').isMongoId().withMessage('The _id must be a Mongo ID!')], Controller.getById)
    .delete([param("id").not().isEmpty().withMessage('An _id must be provided').isMongoId().withMessage('The _id must be a Mongo ID!')], Controller.delete)
    .put(Controller.update)

module.exports = router;