const express = require('express');
const app = express();

//express.json() This is a built-in middleware function in Express. It parses incoming requests with JSON payloads
app.use(express.json());

app.use(express.urlencoded());

const mongoose = require("mongoose");
global.MONGOOSE = mongoose.createConnection("mongodb+srv://dtam:5SNhnBGKPUJTYy2M@cluster0.wsbmj.mongodb.net/DTAM?retryWrites=true&w=majority")

const Sequelize = require("sequelize");
global.SEQUELIZE = new Sequelize('joaoferr_dtam', 'joaoferr_dtam', '5SNhnBGKPUJTYy2M', {
    host: 'www.joaoferreira.eu',
    dialect: 'mysql',
    logging: false
});

app.use("/sql/students", require("./server/Routes/StudentSQLRoutes"));
app.use("/mongo/students", require("./server/Routes/StudentMongoRoutes"));

app.listen(3000, () => {
    console.log(`Server listening on: http://localhost:3000`);
})